#!/usr/bin/python
"""
Read the Arcanum json data files, and build appropriate
GitLab Wiki Markdown content for the site
"""

import json
import math
import os
import requests
import sys
import traceback
from datetime import datetime
from os import path


class Data:
    """
    This class holds the accumulated game data read from the .json files.
    It also holds the code to make category pages such as 'Locales' and 'Dungeons'
    on the wiki
    """
    classes = {}
    dungeons = {}
    enchants = {}
    encounters = {}
    events = {}
    furnitures = {}
    homes = {}
    locales = {}
    materials = {}
    monsters = {}
    potions = {}
    rares = {}
    reagents = {}
    resources = {}
    skills = {}
    spells = {}
    tasks = {}
    upgrades = {}
    weapons = {}
    player = {}
    stressors = {}
    tags = None

    @classmethod
    def load_base_data(cls):
        """
        Load and process the base data files, storing data in class-level
        dictionaries
        """
        with open("../data/resources.json") as f:
            for item in json.load(f):
                cls.resources[item["id"]] = Resource(item)
        with open("../data/locales.json") as f:
            for item in json.load(f):
                cls.locales[item["id"]] = Locale(item)
        with open("../data/encounters.json") as f:
            for item in json.load(f):
                cls.encounters[item["id"]] = Encounter(item)
        with open("../data/dungeons.json") as f:
            for item in json.load(f):
                cls.dungeons[item["id"]] = Dungeon(item)
        with open("../data/monsters.json") as f:
            for item in json.load(f):
                cls.monsters[item["id"]] = Monster(item)
        with open("../data/upgrades.json") as f:
            for item in json.load(f):
                cls.upgrades[item["id"]] = Upgrade(item)
        with open("../data/tasks.json") as f:
            for item in json.load(f):
                cls.tasks[item["id"]] = Task(item)
        with open("../data/weapons.json") as f:
            for item in json.load(f):
                cls.weapons[item["id"]] = Weapon(item)
        with open("../data/enchants.json") as f:
            for item in json.load(f):
                cls.enchants[item["id"]] = Enchant(item)
        with open("../data/materials.json") as f:
            for item in json.load(f):
                cls.materials[item["id"]] = Material(item)
        with open("../data/rares.json") as f:
            for item in json.load(f):
                cls.rares[item["id"]] = Rare(item)
        with open("../data/furniture.json") as f:
            for item in json.load(f):
                cls.furnitures[item["id"]] = Furniture(item)
        with open("../data/homes.json") as f:
            for item in json.load(f):
                cls.homes[item["id"]] = Home(item)
        with open("../data/spells.json") as f:
            for item in json.load(f):
                cls.spells[item["id"]] = Spell(item)
        with open("../data/skills.json") as f:
            for item in json.load(f):
                cls.skills[item["id"]] = Skill(item)
        with open("../data/classes.json") as f:
            for item in json.load(f):
                cls.classes[item["id"]] = Class(item)
        with open("../data/events.json") as f:
            for item in json.load(f):
                cls.events[item["id"]] = Event(item)
        with open("../data/potions.json") as f:
            for item in json.load(f):
                cls.potions[item["id"]] = Potion(item)
        with open("../data/reagents.json") as f:
            for item in json.load(f):
                cls.reagents[item["id"]] = Reagent(item)
        with open("../data/player.json") as f:
            for item in json.load(f):
                cls.player[item["id"]] = Player(item)
        with open("../data/stressors.json") as f:
            for item in json.load(f):
                cls.stressors[item["id"]] = Stressor(item)

    @classmethod
    def load_module_data(cls):
        """
        Load and process the module files by adding them to the
        existing class-level dictionaries
        """
        directory = "../data/modules"
        for module_name in os.listdir(directory):
            module_path = os.path.join(directory, module_name)
            # print(f"Module name: {module_path}")
            with open(module_path) as f:
                module_data = json.load(f)
            # pp.pprint(module_data)
            sym = None
            if "sym" in module_data:
                sym = module_data["sym"]  # winter module, etc
            if "data" in module_data:
                mdata = module_data["data"]
                for key in mdata:
                    if key == "dungeons":
                        for item in mdata[key]:
                            cls.dungeons[item["id"]] = Dungeon(item)
                            cls.dungeons[item["id"]].sym = sym
                    elif key == "monsters":
                        for item in mdata[key]:
                            cls.monsters[item["id"]] = Monster(item)
                            cls.monsters[item["id"]].sym = sym
                    elif key == "upgrades":
                        for item in mdata[key]:
                            cls.upgrades[item["id"]] = Upgrade(item)
                    elif key == "resources":
                        for item in mdata[key]:
                            cls.resources[item["id"]] = Resource(item)
                    elif key == "tasks":
                        for item in mdata[key]:
                            cls.tasks[item["id"]] = Task(item)
                    elif key == "weapons":
                        for item in mdata[key]:
                            cls.weapons[item["id"]] = Weapon(item)
                    elif key == "enchants":
                        for item in mdata[key]:
                            cls.enchants[item["id"]] = Enchant(item)
                    elif key == "materials":
                        for item in mdata[key]:
                            cls.materials[item["id"]] = Material(item)
                    elif key == "rares":
                        for item in mdata[key]:
                            cls.rares[item["id"]] = Rare(item)
                    elif key == "furniture":
                        for item in mdata[key]:
                            cls.furnitures[item["id"]] = Furniture(item)
                            cls.furnitures[item["id"]].sym = sym
                    elif key == "homes":
                        for item in mdata[key]:
                            cls.homes[item["id"]] = Home(item)
                            cls.homes[item["id"]].sym = sym
                    elif key == "spells":
                        for item in mdata[key]:
                            cls.spells[item["id"]] = Spell(item)
                    elif key == "encounters":
                        for item in mdata[key]:
                            cls.encounters[item["id"]] = Encounter(item)
                            cls.encounters[item["id"]].sym = sym
                    elif key == "locales":
                        for item in mdata[key]:
                            cls.locales[item["id"]] = Locale(item)
                            cls.locales[item["id"]].sym = sym
                    elif key == "skills":
                        for item in mdata[key]:
                            cls.skills[item["id"]] = Skill(item)
                    elif key == "classes":
                        for item in mdata[key]:
                            cls.classes[item["id"]] = Class(item)
                    elif key == "events":
                        for item in mdata[key]:
                            cls.events[item["id"]] = Event(item)
                    elif key == "potions":
                        for item in mdata[key]:
                            cls.potions[item["id"]] = Potion(item)
                    elif key == "fupgrades":
                        # Unimplemented by the game as of 2021-03-04
                        pass
                    else:
                        print(f"Unknown module key '{key}'")
                        exit(0)

    @staticmethod
    def generate_pages():
        # pp = pprint.PrettyPrinter(indent=4, sort_dicts=False)
        # pp.pprint(vars(Data))
        Class.category_page()
        #Class.entry_pages()
        #Dungeon.category_page()
        #Enchant.category_page()
        #Encounter.category_page()
        #Encounter.entry_pages()
        #Furniture.category_page()
        #Home.category_page()
        #Locale.category_page()
        #Locale.entry_pages()
        #Monster.category_page()
        #Player.category_page()
        #Player.entry_pages()
        #Potion.category_page()
        #Potion.entry_pages()
        #Tags.category_page()

        # UNFINISHED BELOW HERE #
        # Event needs some discussion
        #Event.category_page()
        
        #Material.category_page()
        #Rare.category_page()
        #Reagent.category_page()
        #Resource.category_page()
        #Skill.category_page()
        #Spell.category_page()
        #Stressor.category_page()
        #Task.category_page()
        #Upgrade.category_page()
        #Weapon.category_page()

    @classmethod
    def id_list_to_wiki(cls, id_list, style="flow"):
        """
        Take a list of game IDs, and return a string of slugs separated by breaks
        :param id_list: list of game IDs
        :param style: 'single' for <br> separated lines, 'flow' for commas
        :return: string of break-separated GitLab markdown slugs
        """
        result_list = []
        for item in id_list:
            suffix = []
            prefix = []
            if type(item) == list:
                prefix = [item[0]]
                item = item[1]
            # devmsg(f"prefix({prefix}) item({item})")
            if item.endswith(".mod.space.max"):
                suffix.insert(0, "Space Max")
                item = item.replace(".mod.space.max", "")
            if item == "inv.max":
                suffix.insert(0, "Space")
                item = "Inventory"
            if item.endswith(".max"):
                suffix.insert(0, "Max")
                item = item.replace(".max", "")
            if item == "element.rate":
                suffix.insert(0, "Rate")
                item = "Element"
            if item.endswith(".rate"):
                suffix.insert(0, "Rate")
                item = item.replace(".rate", "")
            if item.endswith(".keep"):
                suffix.insert(0, "Keep")
                item = item.replace(".keep", "")
            if item.endswith(".exp"):
                suffix.insert(0, "Exp")
                item = item.replace(".exp", "")
                
            if item in cls.resources:
                result_list.append(cls.resources[item].to_slug(prefix=prefix, suffix=suffix))
            elif item in cls.skills:
                if item == "s_travel":
                    suffix.insert(0, "Skill")
                result_list.append(cls.skills[item].to_slug(prefix=prefix, suffix=suffix))
            # elif "s_" + item in cls.skills:
            #    result_list.append(cls.skills["s_" + item].to_slug(prefix=prefix, suffix=suffix))
            elif item in cls.tasks:
                if item == "a_travel":
                    suffix.insert(0, "Task")
                result_list.append(cls.tasks[item].to_slug(prefix=prefix, suffix=suffix))
            elif item in cls.locales:
                result_list.append(cls.locales[item].to_slug(prefix=prefix, suffix=suffix))
            elif item in cls.events:
                result_list.append(cls.events[item].to_slug(prefix=prefix, suffix=suffix))
            elif item in cls.dungeons:
                result_list.append(cls.dungeons[item].to_slug(prefix=prefix, suffix=suffix))
            elif item in cls.classes:
                result_list.append(cls.classes[item].to_slug(prefix=prefix, suffix=suffix))
            elif item in cls.encounters:
                result_list.append(cls.encounters[item].to_slug(prefix=prefix, suffix=suffix))
            elif item in cls.upgrades:
                result_list.append(cls.upgrades[item].to_slug(prefix=prefix, suffix=suffix))
            elif item in cls.monsters:
                result_list.append(cls.monsters[item].to_slug(prefix=prefix, suffix=suffix))
            elif item in cls.rares:
                result_list.append(cls.rares[item].to_slug(prefix=prefix, suffix=suffix))
            elif item in cls.player:
                result_list.append(cls.player[item].to_slug(prefix=prefix, suffix=suffix))
            elif item in cls.furnitures:
                result_list.append(cls.furnitures[item].to_slug(prefix=prefix, suffix=suffix))
            elif item in cls.homes:
                result_list.append(cls.homes[item].to_slug(prefix=prefix, suffix=suffix))
            elif item in cls.reagents:
                result_list.append(cls.reagents[item].to_slug(prefix=prefix, suffix=suffix))
            elif item in cls.spells:
                result_list.append(cls.spells[item].to_slug(prefix=prefix, suffix=suffix))
            elif item in cls.stressors:
                result_list.append(cls.stressors[item].to_slug(prefix=prefix, suffix=suffix))
            elif item in cls.weapons:
                result_list.append(cls.weapons[item].to_slug(prefix=prefix, suffix=suffix))
            elif item in cls.enchants:
                result_list.append(cls.enchants[item].to_slug(prefix=prefix, suffix=suffix))
            elif item.startswith("player."):
                sub_item = item.split(".")[1]
                if item in cls.player:
                    result_list.append(cls.player[sub_item].to_slug(prefix=prefix, suffix=suffix))
                else:
                    result_list.append(sub_item)
            else:
                # No slugs here, we can't find the object..
                if not item.startswith("["):
                    devmsg(f"ID '{item}' not found! HALP!!")
                text = item.title()
                # devmsg(f"text({text}) suffix({suffix}) prefix({prefix})")
                if suffix:
                    text += " " + " ".join(suffix)
                if prefix:
                    for p in prefix:
                        # text.insert(0, p)
                        text += f": {p}"
                # devmsg(f"text: {text}")
                result_list.append(text)
        result_list.sort()
        sep = ", "
        if style == "single":
            sep = ",<br>"
        return sep.join(result_list)
    
    @staticmethod
    def get_object(object_id):
        """
        Provided a valid object id, return the object
        :param object_id: id string
        :return: object, or None if not found
        """
        if object_id in Data.classes:
            return Data.classes[object_id]
        elif object_id in Data.dungeons:
            return Data.dungeons[object_id]
        elif object_id in Data.enchants:
            return Data.enchants[object_id]
        elif object_id in Data.encounters:
            return Data.encounters[object_id]
        elif object_id in Data.events:
            return Data.events[object_id]
        elif object_id in Data.furnitures:
            return Data.furnitures[object_id]
        elif object_id in Data.homes:
            return Data.homes[object_id]
        elif object_id in Data.locales:
            return Data.locales[object_id]
        elif object_id in Data.materials:
            return Data.materials[object_id]
        elif object_id in Data.monsters:
            return Data.monsters[object_id]
        elif object_id in Data.potions:
            return Data.potions[object_id]
        elif object_id in Data.rares:
            return Data.rares[object_id]
        elif object_id in Data.reagents:
            return Data.reagents[object_id]
        elif object_id in Data.resources:
            return Data.resources[object_id]
        elif object_id in Data.skills:
            return Data.skills[object_id]
        elif object_id in Data.spells:
            return Data.spells[object_id]
        elif object_id in Data.tasks:
            return Data.tasks[object_id]
        elif object_id in Data.upgrades:
            return Data.upgrades[object_id]
        elif object_id in Data.weapons:
            return Data.weapons[object_id]
        elif object_id in Data.player:
            return Data.player[object_id]
        elif object_id in Data.stressors:
            return Data.stressors[object_id]
        elif "player." in object_id:
            sub_id = object_id.replace("player.", "")
            return Data.get_object(sub_id)
        
        # If we didn't find it, return None
        # devmsg(f"Object ID '{object_id}' not found!")
        return None


class Wiki:
    """ Base class for the data objects """
    # Default all of the valid attributes in all of the data
    action = None
    actdesc = None
    actname = None
    adj = None
    alias = None
    alter = None
    armor = None
    at = None
    attack = None
    bars = None
    biome = None
    bonus = None
    boss = None
    buy = None
    buyname = None
    cd = None
    choice = None
    color = None
    cost = None
    damage = None
    defense = None
    desc = None
    disable = None
    dist = None
    dodge = None
    dot = None
    effect = None
    enchants = None
    encs = None
    enemies = None
    every = None
    evil = None
    evilamt = None
    exclude = None
    fill = None
    flavor = None
    group = None
    hands = None
    hide = None
    hp = None
    id = None
    immune = None
    kind = None
    length = None
    level = None
    lock = None
    locked = None
    log = None
    loot = None
    material = None
    max = None
    mod = None
    name = None
    need = None
    noproc = None
    only = None
    owned = None
    perpetual = None
    priceMod = None
    properties = None
    rate = None
    reflect = None
    regen = None
    repeat = None
    require = None
    resist = None
    result = None
    reverse = None
    run = None
    runmod = None
    save = None
    scale = None
    school = None
    secret = None
    sell = None
    silent = None
    slot = None
    spawns = None
    speed = None
    spells = None
    stack = None
    start = None
    stat = None
    sym = None
    tags = None
    title = None
    tohit = None
    type = None
    unique = None
    unit = None
    unused = None
    use = None
    val = None
    verb = None
    warn = None
    weight = None

    def __init__(self, json_data):
        for key, val in json_data.items():
            if key == "distance":  # TODO: combat tutorial goof
                key = "dist"
            if hasattr(self, key):
                setattr(self, key, val)
            else:
                print(f"{type(self)} has no key '{key}'")
                exit(1)
            if key == "tags":
                if "," in val:
                    tag_list = val.split(",")
                    for tag in tag_list:
                        Tags.add(tag, self.id)
                else:
                    Tags.add(val, self.id)

    @classmethod
    def category_page(cls):
        """ Default untemplated category page """
        output = "This page is a default list of the items in this category.<br>\n"
        output += "If it doesn't meet your needs, please let a developer "
        output += "(probably 'simple') know, and they'll get around to updating it.<br>\n\n"
        domain = cls.__name__.lower()
        if domain == "class":
            domain = "classes"
        elif domain == "player":
            pass
        else:
            domain += "s"
        title = domain.title()
        print(f"Generating generic page for '{title}'")
        domain_data = getattr(Data, domain)
        item_list = []
        for key in domain_data.keys():
            item = domain_data[key]
            if getattr(item, "secret"):
                continue  # TODO: Figure out spoiler tags; until then, skip
            item_list.append(item.to_slug())
        item_list.sort()
        output += "<br>\n".join(item_list)
        # print(output)
        wikipost(title, output)
    
    def entry_page(self):
        """ Default untemplated entry page """
        key_to_title = {
            "actname": "Action Name",
            "actdesc": "Action Description",
            "alias": "Alias",
            "buy": "Cost to Buy",
            "cost": "Cost to acquire",
            "desc": "Description",
            "disable": "Class disables",
            "effect": "Effects",
            "encs": "Encounters",
            "flavor": "Flavor",
            "hide": "Hidden",
            "id": "ID",
            "length": "Length",
            "level": "Level",
            "locked": "Locked",
            "log": "Log Entry",
            "loot": "Loot",
            "max": "Maximum",
            "mod": "Modifiers",
            "name": "Name",
            "need": "Need",
            "rate": "Rate",
            "require": "Requirements",
            "result": "Result",
            "run": "Run Cost",
            "secret": "Secret",
            "sell": "Gains from selling",
            "stack": "Stackable",
            "start": "Start Announcement",
            "stat": "Statistics",
            "sym": "Symbol",
            "tags": "Tags",
            "title": "Title",
            "unit": "Only Integer Values",
            "use": "Effect of using",
            "val": "Value",
            "warn": "Pop-up Warning",
        }
        resolve_list = [
            "buy", "cost", "disable", "effect", "encs", "log", "loot", "mod",
            "need", "require", "result", "run", "start", "tags", "use",
        ]
        output = ""
        for key in vars(self):
            if key in ["id", "name"]:
                continue  # Skip these fields
            if self.secret:
                continue  # TODO: fix this later
            title = key_to_title[key]
            val = getattr(self, key)
            t = type(val)
            # devmsg(f"key type is {t}")
            if key == "require":
                output += f"**{title}**: " + self.get_requirements() + "\n\n"
                continue
            if key not in resolve_list:
                if key == "name":
                    val = self.get_title()
                if val != "" or val == {}:
                    output += f"**{title}**: {val}\n\n"
            elif t == dict:
                items = []
                output += f"**{title}**\n\n"
                for key2, val2 in val.items():
                    thing = Data.id_list_to_wiki([key2])
                    output += f"- {thing}: {val2}\n\n"
            elif t == str:
                thing = Data.id_list_to_wiki([val])
                output += f"**{title}**: {thing}\n\n"
            elif t == list:
                if not val:  # skip "disable": [],
                    continue
                output += f"**{title}**\n\n"
                for item in val:
                    thing = Data.id_list_to_wiki([item])
                    output += f"- {thing}\n\n"
            else:
                devmsg(f"key({key}) val({val}) invalid val type ({type(val)})")
                raise Exception(f"Unknown value type: {type(val)}")
        
        # output += "\n---\n*This page was automatically generated from a default template.*\n"
        return output

    def get_all_rewards(self):
        """
        Get this object's skill experience rewards
        :return: list of skill IDs
        """
        tmp = dict()
        if self.result is not None:
            for i in self.result:
                tmp[i] = True
        if self.effect is not None:
            for i in self.effect:
                tmp[i] = True
        if self.loot is not None:
            for i in self.loot:
                tmp[i] = True
        if self.mod is not None:
            if type(self.mod) == str:
                tmp[self.mod] = True
            else:
                for i in self.mod:
                    tmp[i] = True
        if self.encs is not None:
            for i in self.encs:
                items = Data.encounters[i].get_all_rewards()
                for item in items:
                    tmp[item] = True
        if self.spawns is not None:
            for i in self.spawns:
                if type(i) == dict:
                    i = i["ids"]  # fix some weird jazid thing
                if type(i) == list:
                    for i2 in i:
                        items = Data.monsters[i2].get_all_rewards()
                        for item in items:
                            tmp[item] = True
                else:
                    if i == "lgbird":
                        continue  # doesn't exist
                    if i == "level" or i == "range":
                        continue  # non-trackable format: "spawns":{"level":"15~23","range":2}
                    items = Data.monsters[i].get_all_rewards()
                    for item in items:
                        tmp[item] = True
        all_list = list(tmp.keys())
        all_list.sort()
        return all_list

    def get_bosses(self):
        """
        Get a list of all bosses for this object
        :return: list of boss IDs
        """
        tmp = dict()
        # print(f"getting bosses for {self.id}")
        if self.boss is not None:
            boss = self.boss
            if type(boss) == str:
                boss = [boss]
            elif type(boss) == dict:
                boss = list(boss.values())
            for i in boss:
                # print(f"boss is {i}")
                if type(i) == list:
                    for i2 in i:
                        tmp[i2] = True
                else:
                    tmp[i] = True
        boss_list = list(tmp.keys())
        boss_list.sort()
        return boss_list

    def get_buy_costs(self):
        """
        Get the costs of buying this object
        :return: list of IDs
        """
        cost_list = []
        if self.buy is not None:
            for key, val in self.buy.items():
                cost_list.append([str(val), key])
        # print(f"run cost for {self.id}: {cost_list}")
        return cost_list
    
    def get_dist(self):
        """ Returns the distance to a locale or dungeon """
        if self.dist is not None:
            return self.dist
        dist = 0
        try:
            dist = math.ceil(4.4 * math.exp(0.30 * self.get_level()))
        except TypeError as e:
            print(f"Error: {e}")
            print(vars(self))
            exit(1)
        return dist

    def get_effects(self):
        """
        Get the effects of running this object
        :return: list of [value, ID]
        """
        effect_list = []
        if self.effect is not None:
            for key, val in self.effect.items():
                effect_list.append([str(val), key])
        return effect_list

    def get_encounters(self):
        """
        Get a list of all encounters for this object
        :return: list of encounter IDs
        """
        tmp = dict()
        if self.spawns is not None:
            for i in self.spawns:
                if type(i) == dict:
                    i = i["ids"]  # fix some weird jazid thing
                if type(i) == list:
                    for i2 in i:
                        tmp[i2] = True
                else:
                    if i == "lgbird":
                        continue  # doesn't exist
                    if i == "level" or i == "range":
                        continue  # untrackable format: "spawns":{"level":"15~23","range":2}
                    tmp[i] = True
        enc_list = list(tmp.keys())
        enc_list.sort()
        return enc_list

    def get_exp_rewards(self):
        """
        Get this object's skill experience rewards
        :return: list of skill IDs
        """
        tmp = dict()
        if self.result is not None:
            for i in self.result:
                if i == "player.exp":
                    continue  # player exp
                if i.endswith(".exp"):
                    key = i.replace(".exp", "")
                    tmp[key] = True
        if self.effect is not None:
            for i in self.effect:
                if i == "player.exp":
                    continue  # player exp
                if i.endswith(".exp"):
                    key = i.replace(".exp", "")
                    tmp[key] = True
        if self.loot is not None:
            for i in self.loot:
                if i.endswith(".exp"):
                    key = i.replace(".exp", "")
                    tmp[key] = True
        if self.mod is not None:
            for i in self.mod:
                if i.endswith(".exp"):
                    key = i.replace(".exp", "")
                    tmp[key] = True
        if self.encs is not None:
            for i in self.encs:
                if i == "movingsnow":
                    # This doesn't really exist
                    continue
                items = Data.encounters[i].get_exp_rewards()
                for item in items:
                    tmp[item] = True
        if self.spawns is not None:
            for i in self.spawns:
                if type(i) == dict:
                    i = i["ids"]  # fix some weird jazid thing
                if type(i) == list:
                    for i2 in i:
                        items = Data.monsters[i2].get_exp_rewards()
                        for item in items:
                            tmp[item] = True
                else:
                    if i == "lgbird":
                        continue  # doesn't exist
                    if i == "level" or i == "range":
                        continue  # non-trackable format: "spawns":{"level":"15~23","range":2}
                    items = Data.monsters[i].get_exp_rewards()
                    for item in items:
                        tmp[item] = True
        exp_list = list(tmp.keys())
        exp_list.sort()
        return exp_list
    
    def get_length(self):
        """ Returns the number of iterations a locale or dungeon has """
        if self.length is not None:
            return self.length
        else:
            return 5 * self.get_level()

    def get_level(self):
        if self.level is not None:
            return self.level
        else:
            return 1

    def get_max_rewards(self):
        """
        Get this Locale's skill max rewards
        :return: list of skill IDs
        """
        tmp = {}
        if self.result is not None:
            for i in self.result:
                if i.endswith(".max"):
                    key = i.replace(".max", "")
                    tmp[key] = True
        if self.loot is not None:
            for i in self.loot:
                if i.endswith(".max"):
                    key = i.replace(".max", "")
                    tmp[key] = True
        if self.mod is not None:
            for i in self.mod:
                if i.endswith(".max"):
                    key = i.replace(".max", "")
                    tmp[key] = True
        if self.effect is not None:
            for i in self.effect:
                if i.endswith(".max"):
                    key = i.replace(".max", "")
                    tmp[key] = True
        if self.encs is not None:
            for i in self.encs:
                if i == "movingsnow":
                    # This doesn't really exist
                    continue
                items = Data.encounters[i].get_max_rewards()
                for item in items:
                    tmp[item] = True
        if self.spawns is not None:
            for i in self.spawns:
                if type(i) == dict:
                    i = i["ids"]  # fix some weird jazid thing
                if type(i) == list:
                    for i2 in i:
                        items = Data.monsters[i2].get_max_rewards()
                        for item in items:
                            tmp[item] = True
                else:
                    if i == "lgbird":
                        continue  # doesn't exist
                    if i == "level" or i == "range":
                        continue  # untrackable format: "spawns":{"level":"15~23","range":2}
                    items = Data.monsters[i].get_max_rewards()
                    for item in items:
                        tmp[item] = True
    
        max_list = list(tmp.keys())
        max_list.sort()
        return max_list

    def get_mods(self):
        """
        Get the mods of running this object
        :return: list of [value, ID]
        """
        mod_list = []
        if self.mod is not None:
            for key, val in self.mod.items():
                val = str(val)
                if key == "rest.effect.stamina":
                    mod_list.append([val, "[Rest Stamina](rest)"])
                elif key == "t_runes.max":
                    mod_list.append([val, "[Runestone Max](runestones)"])
                elif key == "notoriety.rate":
                    mod_list.append([val, "[Notoriety Rate](fame)"])
                elif key == "codices.mod.research.max":
                    mod_list.append([val, "[Codices Research Max](codices)"])
                elif key == "tomes.mod.research.max":
                    mod_list.append([val, "[Tomes Research Max](tomes)"])
                elif key == "tomes.mod.arcana.max":
                    mod_list.append([val, "[Tomes Arcana Max](tomes)"])
                elif key == "study.effect.research":
                    mod_list.append([val, "[Studying Research](study)"])
                elif key == "assemblepuppet.cost.gold":
                    mod_list.append([val, "[Puppet Gold Cost](assemblepuppet)"])
                elif key == "assemblepuppet.cost.research":
                    mod_list.append([val, "[Puppet Research Cost](assemblepuppet)"])
                elif key == "assemblepuppet.result.puppets":
                    mod_list.append([val, "[Puppet Quantity Bonus](assemblepuppet)"])
                elif key == "assembleautomata.cost.gold":
                    mod_list.append([val, "[Automata Gold Cost](assembleautomata)"])
                elif key == "assembleautomata.cost.research":
                    mod_list.append([val, "[Automata Research Cost](assembleautomata)"])
                elif key == "assembleautomata.cost.runestones":
                    mod_list.append([val, "[Automata Runestone Cost](assembleautomata)"])
                elif key == "assemblemachina.cost.gold":
                    mod_list.append([val, "[Machina Gold Cost](machinaassembly)"])
                elif key == "assemblemachina.cost.research":
                    mod_list.append([val, "[Machina Research Cost](machinaassembly)"])
                elif key == "assemblemachina.result.machinae":
                    mod_list.append([val, "[Machina Quantity Bonus](machinaassembly)"])
                elif key == "iceflame.attack.dot.duration":
                    mod_list.append([val, "[Iceflame Dot Duration](iceflame)"])
                elif key == "livingsnow.mod.livingsnow.rate":
                    mod_list.append([val, "[Livingsnow Rate](livingsnow)"])
                elif key == "hearth.cost.firegem":
                    mod_list.append([val, "[Hearth Firegem Cost](hearth)"])
                elif key == "liquifier.mod.managem.rate":
                    mod_list.append([val, "[Liquifier Managem Rate](liquifier)"])
                elif key == "machinaautomation.mod.space":
                    mod_list.append([val, "[Machina Automation Space](machina)"])
                elif key == "machinagarage.mod.space":
                    mod_list.append([val, "[Machina Garage Space](machina)"])
                elif key == "puppetautomation.mod.space":
                    mod_list.append([val, "[Puppet Automation Space](puppets)"])
                elif key == "puppetworkshop.mod.space":
                    mod_list.append([val, "[Puppet Workshop Space](puppets)"])
                elif key == "shapinglab.mod.space":
                    mod_list.append([val, "[Shaping Lab Space](shapinglab)"])
                else:
                    mod_list.append([str(val), key])
        return mod_list
    
    def get_requirements(self):
        """
        Get the things required to be able to do this object
        :return: string of GitLab markdown text
        """
        elements = []
        if self.require is None:
            return ""
        elif type(self.require) == str:
            raw_line = self.require.replace(">=", " ≥ ").replace("<=", " ≤ ")\
                .replace(">", " > ").replace("<", " < ").replace("+", " + ")\
                .replace("==", " = ").replace("(", "( ").replace(")", " )")\
                .replace("&&", " and ").replace("||", " or ")
            for term in raw_line.split():
                # devmsg(f"attempting to resolve {self.id} term '{term}'...")
                entity = term  # default if not resolvable
                if term.startswith("g."):
                    subterm = term.replace("g.", "")
                    # devmsg(f"attempting to resolve subterm '{subterm}'...")
                    if subterm.startswith("player."):
                        subsubterm = subterm.replace("player.", "")
                        slug = Data.id_list_to_wiki([subsubterm])
                    elif subterm.endswith(".value"):  # g.apprentice.value
                        subsubterm = subterm.replace(".value", "")
                        slug = Data.id_list_to_wiki([subsubterm])
                    else:
                        slug = Data.id_list_to_wiki([subterm])
                    # devmsg(f"slug for {subterm} is {slug}")
                    entity = slug
                elif term.startswith("evt_"):
                    entity = Data.id_list_to_wiki([term])
                elif term in Data.events:
                    entity = Data.id_list_to_wiki([term])
                elif term in Data.resources:
                    entity = Data.id_list_to_wiki([term])
                elif term in Data.skills:
                    entity = Data.id_list_to_wiki([term])
                elif term in Data.tasks:
                    entity = Data.id_list_to_wiki([term])
                elif term in Data.locales:
                    entity = Data.id_list_to_wiki([term])
                elif term in Data.dungeons:
                    entity = Data.id_list_to_wiki([term])
                elif term in Data.classes:
                    entity = Data.id_list_to_wiki([term])
                elif term in Data.upgrades:
                    entity = Data.id_list_to_wiki([term])
                elif term.startswith("tier"):
                    entity = Data.id_list_to_wiki([term])
                elif term == "mustylibrary":
                    entity = Data.id_list_to_wiki([term])
                elif term in ["≥", "≤", "<", ">", "=", "+", "and", "or", "+", "(", ")"]:
                    pass
                elif term.isnumeric():
                    pass
                else:
                    devmsg(f"{self.id} term '{term}' not resolved!")
                    exit(1)
                elements.append(entity)
            # devmsg(elements)
            output = " ".join(elements).replace("( ", "(").replace(" )", ")")
            # devmsg(f"output: {output}")
            return output
        elif type(self.require) == list:
            # devmsg(f"{self.id} require is a list")
            for i in self.require:
                elements.append(Data.id_list_to_wiki([i]))
            output = ",<br>".join(elements)
            # devmsg(f"output: {output}")
            return output
        else:
            devmsg(f"{self.id} require unknown: {self.require}")
            exit(1)

    def get_resource_rewards(self):
        """
        Get this object's resource rewards
        :return: list of resource IDs
        """
        tmp = dict()
        if self.result is not None:  # {'arcana': 0.1, 'research': 10}
            for i in self.result:
                if i in Data.resources:
                    tmp[i] = True
                elif i in Data.rares:
                    tmp[i] = True
        if self.loot is not None:
            for i in self.loot:
                if i in Data.resources:
                    tmp[i] = True
                elif i in Data.rares:
                    tmp[i] = True
        if self.encs is not None:
            for i in self.encs:
                if i == "movingsnow":
                    # This doesn't really exist
                    continue
                items = Data.encounters[i].get_resource_rewards()
                for item in items:
                    tmp[item] = True
        if self.spawns is not None:
            for i in self.spawns:
                if type(i) == dict:
                    i = i["ids"]  # fix some weird jazid thing
                if type(i) == list:
                    for i2 in i:
                        items = Data.monsters[i2].get_resource_rewards()
                        for item in items:
                            tmp[item] = True
                else:
                    if i == "lgbird":
                        continue  # doesn't exist
                    if i == "level" or i == "range":
                        continue  # untrackable format: "spawns":{"level":"15~23","range":2}
                    if i in Data.monsters:
                        items = Data.monsters[i].get_resource_rewards()
                        for item in items:
                            tmp[item] = True
                    else:
                        print(f"item '{i}' not in Data.monsters!")
                        exit(1)
        resource_list = list(tmp.keys())
        resource_list.sort()
        return resource_list

    def get_run_costs(self):
        """
        Get the costs of running this object
        :return: list of IDs
        """
        cost_list = []
        if self.run is not None:
            for key, val in self.run.items():
                cost_list.append([str(val), key])
        if self.cost is not None:
            for key, val in self.cost.items():
                cost_list.append([str(val), key])
        # print(f"run cost for {self.id}: {cost_list}")
        return cost_list

    def get_symbol(self):
        """ Return the emoji symbol for this object, if any """
        if self.sym is not None:
            symbol = self.sym
        elif type(self) == Locale:  # Defaults to tree
            symbol = "🌳"
        elif type(self) == Dungeon:  # Defaults to crossed swords
            symbol = "⚔"
        else:
            return ""
        return "<span style=\"font-size:2em;\">" + symbol + "</span>"
        
    def get_title(self):
        """ Returns the title of the object """
        title = self.id
        if self.name is not None:
            title = self.name
        title = title.title().replace("'S", "'s").replace(" Of ", " of ")\
            .replace(" The ", " the ").replace(" Iv", " IV").replace(" Iii", " III")\
            .replace(" Ii", " II")
        return title

    def get_unlocks(self):
        """
        Get a list of things which require this object to unlock
        :return: list of IDs
        """
        tmp = dict()
        # Check unlocks
        # TODO: other types perhaps?
        for item_id in Data.locales:
            item = Data.locales[item_id]
            if self.id in item.get_requires():
                tmp[item_id] = True
        for item_id in Data.skills:
            item = Data.skills[item_id]
            if self.id in item.get_requires():
                tmp[item_id] = True
        for item_id in Data.tasks:
            item = Data.tasks[item_id]
            if self.id in item.get_requires():
                tmp[item_id] = True
        unlock_list = list(tmp.keys())
        unlock_list.sort()
        return unlock_list

    def to_slug(self, prefix=[], suffix=[]):
        """ Generates and returns the object's page link """
        texts = [self.get_title()]
        slug = self.get_title().replace(" ", "-").replace("?", "_")
        prefixes = ""
        if suffix:
            texts.extend(suffix)
        if prefix:  # quantity really
            for p in prefix:
                # texts.insert(0, prefix)
                prefixes += f": {p}"
        # print(f"texts: {texts}")
        text = " ".join(texts)
        output = f"[{text}]({slug}){prefixes}"
        # print(f"generating slug for {self.id} a {type(self)} ({output})")
        # if self.id == "research":
        #     print(f"slug input for {self.id}: prefix: {prefix} suffix: {suffix} output: {output}")
        return output


class Class(Wiki):
    """ The Class class """
    
    def __init__(self, json_data):
        super().__init__(json_data)
    
    @staticmethod
    def category_page():
        """ Generate -/wikis/Classes """
        output = \
            "|Name|Requirements|Costs|Benefits|Unlocks|\n" \
            "|:---|:-----------|:---:|:-------|:------|\n"
        tmp = []
        for class_id in Data.classes.keys():
            klass = Data.classes[class_id]
            if klass.secret is not None:
                continue  # TODO: Skip for now
            tmp.append(klass.category_line() + "\n")
        tmp.sort()
        output += "".join(tmp)
        wikipost("Classes", output)
    
    def category_line(self):
        """
        Transform this Class object into a line of Markdown text for the Classes page
        :return: string of gitlab markdown text
        """
        symbol = self.get_symbol()
        title = self.to_slug()
        reqs = self.get_requirements()
        costs = Data.id_list_to_wiki(self.get_run_costs(), style="single")
        benefits = Data.id_list_to_wiki(self.get_all_rewards(), style="single")
        unlocks = Data.id_list_to_wiki(self.get_unlocks(), style="single")
        return f"|{title}<br>{symbol}|{reqs}|{costs}|{benefits}|{unlocks}|"

    @classmethod
    def entry_pages(cls):
        """
        Generate a page for each Class
        :return: None
        """
        # for key in ["blueadept"]:
        for key in Data.classes.keys():
            obj = Data.classes[key]
            if obj.secret:
                # TODO: Something better than skipping secrets
                continue
            title = obj.get_title()
            # Get requirement chain in mermaid format
            markup = obj.get_requirement_chain()
            markup += obj.entry_page()
            wikipost(title, markup)
    
    def get_requirement_chain(self):
        """
        Get all prerequisites for this class
        :return Mermaid markup string
        """
        # devmsg(self.id)
        if self.require:
            requires = self.require.replace("g.tier0>0&&", "").replace("g.tier1>0&&", "")\
                .replace("g.tier2>0&&", "").replace("g.tier3>0&&", "")\
                .replace("g.tier4>0&&", "").replace("g.tier5>0&&", "")\
                .replace("&&g.tier1==0", "").replace("&&g.tier2==0", "")\
                .replace("&&g.tier3==0", "").replace("&&g.tier4==0", "")\
                .replace("&&g.tier5==0", "").replace("&&g.tier6==0", "")\
                .replace("&&g.evil==0", " and not Evil")
            # devmsg(f"Requires: {requires}")
            requires = requires.replace(">=", " ≥ ").replace("<=", " ≤ ")\
                .replace("g.", "").replace("&&", " and ").replace("||", " or ") \
                .replace(">", " > ").replace("+", " + ")\
                .replace("(", " ( ").replace(")", " ) ")
            # devmsg(f"Requires: {requires}")
        else:
            requires = ""
        
        # Split, resolve, and join requirements
        items = []
        for item in requires.split():
            obj = Data.get_object(item)
            if obj is not None:
                items.append(obj.get_title())
            else:
                items.append(item)
        # devmsg(f"items: {items}")
        requires = " ".join(items)
        requires = requires.replace(" and ", ",<br>")\
            .replace("( ", "(").replace(" )", ")")
        
        # devmsg(f"Requires: {requires}")
        
        tier = 0
        if self.tags is not None:
            # devmsg(f"Tags: {self.tags}")
            if "t_tier" in self.tags:
                tier = int(self.tags.replace("t_tier", ""))
        
        mermaid = ""
        if tier > 0:
            prev = tier - 1
            mermaid = f"```mermaid\n"
            mermaid += f"graph LR\n"
            mermaid += f"  t{prev}{{Tier {prev} Class}}\n"
            mermaid += f"  t{prev}r([\"{requires}\"])\n"
            mermaid += f"  t{prev} ---> t{prev}r ---> {self.id}\n"
            mermaid += f"  subgraph tier{tier} [Tier {tier}]\n"
            mermaid += f"    {self.id}([{self.get_title()}])\n"
            mermaid += f"  end\n"
            mermaid += f"```\n"
        
        # devmsg(f"mermaid:\n{mermaid}")
        return mermaid
    
    @staticmethod
    def get_classes_for_tier(tier):
        """
        Get the classes for integer tier input
        :param tier: integer tier number
        :return: array of class ids
        """
        class_ids = []
        tier_tag = f"tier{tier}"
        for class_id in Data.classes.keys():
            tags = Data.classes[class_id].tags
            if tags and tier_tag in tags:
                class_ids.append(class_id)
        return class_ids


class Dungeon(Wiki):
    """ The Dungeon class """
    def __init__(self, json_data):
        super().__init__(json_data)

    @staticmethod
    def category_page():
        """ Generate -/wikis/Dungeons """
        output = \
            "|Name|Level|Length|Distance|Requirement|Consumption|Reward|Encounters|Boss|\n" \
            "|:---|:---:|:----:|:------:|:----------|:----------|:-----|:---------|:---|\n"
        tmp = []
        for dungeon_id in Data.dungeons.keys():
            dungeon = Data.dungeons[dungeon_id]
            tmp.append(dungeon.category_line() + "\n")
        tmp.sort()
        output += "".join(tmp)
        wikipost("Dungeons", output)
        
    def category_line(self):
        """
        Transform this Dungeon object into a line of Markdown text
        :return: string of gitlab markdown text
        """
        symbol = self.get_symbol()
        title = self.to_slug()
        level = self.get_level()
        length = self.get_length()
        dist = self.get_dist()
        requirements = self.get_requirements()
        costs = Data.id_list_to_wiki(self.get_run_costs())
        rewards = Data.id_list_to_wiki(self.get_resource_rewards())
        encounters = Data.id_list_to_wiki(self.get_encounters())
        bosses = Data.id_list_to_wiki(self.get_bosses())
        return f"|{title}<br>{symbol}|{level}|{length}|{dist}|{requirements}|{costs}|{rewards}|{encounters}|{bosses}|"


class Enchant(Wiki):
    """ The Enchant class """
    def __init__(self, json_data):
        super().__init__(json_data)

    @staticmethod
    def category_page():
        """ Generate -/wikis/Enchants """
        nm = "Name"
        rq = "Required"
        bu = "Buy Cost"
        le = "Level"
        sl = "Slot"
        ef = "Effect"
        cc = "Cast Cost"
        output = \
            f"|{nm}|{rq}|{bu}|{le}|{sl}|{ef}|{cc}|\n" \
            f"|:---|:---|:---|:--:|:---|:---|:---|\n"
        tmp = []
        for enchant_id in Data.enchants.keys():
            enchant = Data.enchants[enchant_id]
            tmp.append(enchant.category_line() + "\n")
        tmp.sort()
        output += "".join(tmp)
        wikipost("Enchants", output)

    def category_line(self):
        """
        Transform this Enchant object into a line of Markdown text
        :return: string of gitlab markdown text
        """
        symbol = self.get_symbol()
        title = self.to_slug()
        if self.desc is not None and self.desc != "":
            title += f"<br>*{self.desc}*"
        req = self.get_requirements()
        lev = self.get_level()
        buy = Data.id_list_to_wiki(self.get_buy_costs(), style="single")
        slt = self.get_slots()
        efc = self.get_alters()
        cst = Data.id_list_to_wiki(self.get_run_costs(), style="single")
        return f"|{symbol} {title}|{req}|{buy}|{lev}|{slt}|{efc}|{cst}|"
    
    def get_alters(self):
        """
        Return a nice markup text for alteration effects
        :return: GitLab markdown text
        """
        alters = []
        for key, val in self.alter.items():
            # devmsg(f"({self.id}) {key}, {val}")
            if key.startswith("mod.player.resist."):
                subkey = key.replace("mod.player.resist.", "").title()
                key = subkey + " Resist"
            elif key == "attack.bonus":  key = "Attack Bonus"
            elif key == "attack.damage": key = "Attack Damage"
            elif key == "attack.kind":   key = "Attack"
            elif key == "attack.tohit":  key = "To Hit"
            elif key == "mod.lifedrain.attack.dot.damage.min":
                key = "[Lifedrain](lifedrain) Min Damage"
            elif key == "mod.lifedrain.attack.dot.damage.max":
                key = "[Lifedrain](lifedrain) Max Damage"
            elif key == "mod.blast.attack.dot.damage.min":
                key = "[Blast](blast) Min Damage"
            elif key == "mod.blast.attack.dot.damage.max":
                key = "[Blast](blast) Max Damage"
            elif key == "mod.trapsoul.result.souls.max":
                key = "[Trap Soul](trapsoul) Max Souls"
            elif key == "attack.action.targets":
                if val == "ally":
                    key = "Targets Allies"
                    val = ""
                else:
                    key = "Attack Target"
            elif key == "attack.dot.targets":
                key = "Targets Allies"
                val = ""
            elif key == "attack.dot.mod.dmg.min":
                key = "Dot Min Damage"
            elif key == "attack.dot.mod.dmg.max":
                key = "Dot Max Damage"
            elif key == "attack.action.result.hp.min":
                key = "HP Minimum"
            elif key == "attack.action.result.hp.max":
                key = "HP Maximum"
            elif key == "element.rate":
                key = "Elemental Mana Rate"
            elif key == "mod.prismatic.rate":
                key = "Prismatic Mana Rate"
            elif key == "mod.stress.rate":
                key = "Stress Rate"
            elif key == "mod.stress.max":
                key = "Stress Max"
            elif key == "mod.focus.cost.mana":
                key = "[Focus](focus) Mana Cost"
            elif key == "mod.focus.result.runner.exp":
                key = "[Focus](focus) Exp"
            elif key == "mod.player.bonuses.mana":
                key = "Bonus Mana"
            elif key == "mod.spelllist.max":
                key = "Max Spell List"
            elif key.startswith("mod."):
                key = key.replace("mod.", "")
            name = Data.id_list_to_wiki([key])
            indicator = "+"
            # devmsg(f"val type is {type(val)}")
            if (type(val) == int or type(val) == float) and val < 0:
                indicator = ""
            elif val == "":
                indicator = ""
            elif type(val) == str:
                if not val.endswith("%"):
                    val = Data.id_list_to_wiki([val])
            alters.append(f"{indicator}{val} {name}")
        return ",<br>".join(alters)
        
    def get_slots(self):
        """ Return a nice markup text for slots """
        s = self.only
        slots = []
        if "," in s:
            for i in s.split(","):
                slots.append(self.get_slot_name(i))
        else:
            slots.append(self.get_slot_name(s))
        # devmsg(f"slots: {slots}")
        return ",<br>".join(slots)
    
    @staticmethod
    def get_slot_name(item):
        """
        Given a slot name, find a good title for it
        :return: string of normal titlecase text
        """
        if item in Data.weapons:
            return Data.weapons[item].get_title()
        else:
            # devmsg(f"no title for '{item}'")
            return item.title()


class Encounter(Wiki):
    """ The Encounter class """
    def __init__(self, json_data):
        super().__init__(json_data)

    @staticmethod
    def category_page():
        """ Generate -/wikis/Encounters """
        output = \
            "|Name|Effects|Stresses|Rewards|\n" \
            "|:---|:------|:-------|:------|\n"
        tmp = []
        for encounter_id in Data.encounters.keys():
            encounter = Data.encounters[encounter_id]
            tmp.append(encounter.category_line() + "\n")
        tmp.sort()
        output += "".join(tmp)
        wikipost("Encounters", output)

    def category_line(self):
        """
        Transform this Encounter object into a line of Markdown text
        :return: string of gitlab markdown text
        """
        symbol = self.get_symbol()
        title = self.to_slug()
        if self.desc is not None and self.desc != "":
            title += f"<br>*{self.desc}*"
        # effects = Data.id_list_to_wiki(self.get_effects())
        # rewards = Data.id_list_to_wiki(self.get_resource_rewards())
        e, s, r = self.get_things()
        effects = Data.id_list_to_wiki(e, style="single")
        stresses = Data.id_list_to_wiki(s, style="single")
        rewards = Data.id_list_to_wiki(r, style="single")
        return f"|{symbol} {title}|{effects}|{stresses}|{rewards}|"
    
    def get_things(self):
        """
        Build lists of effects, stresses, and rewards for this Encounter
        :return: array of 3 arrays
        """
        effects = []
        stresses = []
        rewards = []
        if self.effect is not None:
            for key, val in self.effect.items():
                if key == "wear":  # typo
                    key = "weary"
                # print(f"key({key}) val({val})")
                if key in Data.stressors:
                    stresses.append([val, key])
                elif key in Data.resources:
                    effects.append([val, key])
                elif key in Data.player:
                    effects.append([val, key])
                elif key.startswith("player."):
                    key = key.replace("player.", "")
                    if key in Data.player:
                        effects.append([val, key])
                    else:
                        print(F"key 'player {key}' not found in player")
                        exit(1)
                elif key.endswith(".exp"):
                    key2 = key.replace(".exp", "")
                    if key2 in Data.skills:
                        effects.append([val, key])
                    elif "s_" + key2 in Data.skills:
                        effects.append([val, key])
                    else:
                        print(f"exp key '{key2}' not found in skills")
                        exit(1)
                elif key.endswith(".rate"):
                    key = key.replace(".rate", "")
                    if key in Data.skills:
                        effects.append([val, key])
                    else:
                        print(f"rate key '{key}' not found in skills")
                        exit(1)
                elif key.endswith(".max"):
                    key = key.replace(".max", "")
                    if key in Data.skills:
                        effects.append([val, key])
                    elif key in Data.resources:
                        effects.append([val, key])
                    else:
                        print(f"max key '{key}' not found in skills/resource")
                        exit(1)
                elif key == "stress":
                    # TODO: 'stress' is a tag, fix plz
                    stresses.append([val, key])
                else:
                    print(f"I don't know about effect '{key}'")
                    exit(1)
        
        if self.result is not None:
            for key, val in self.result.items():
                # print(f"{self.id} results key({key}) val({val})")
                if key in Data.resources:
                    rewards.append([val, key])
                elif key in Data.events:
                    rewards.append([val, key])
                elif key.endswith(".max"):
                    key2 = key.replace(".max", "")
                    if key2 in Data.skills:
                        rewards.append([val, key])
                    elif key2 in Data.resources:
                        rewards.append([val, key])
                    else:
                        print(f"max key '{key}' not found in skills/resource")
                        exit(1)
                elif key.endswith(".rate"):
                    key = key.replace(".rate", "")
                    if key in Data.skills:
                        rewards.append([val, key])
                    elif key in Data.resources:
                        rewards.append([val, key])
                    else:
                        print(f"rate key '{key}' not found in skills")
                        exit(1)
                elif key.endswith(".exp"):
                    key2 = key.replace(".exp", "")
                    if key2 in Data.skills:
                        rewards.append([val, key])
                    elif "s_" + key2 in Data.skills:
                        rewards.append([val, key])
                    else:
                        print(f"exp key '{key2}' not found in skills")
                        exit(1)
                else:
                    print(f"result key '{key}' not found in resources")
                    exit(1)
        
        return effects, stresses, rewards

    @classmethod
    def entry_pages(cls):
        """
        Generate a page for each Encounter
        :return: None
        """
        for key in Data.encounters.keys():
            obj = Data.encounters[key]
            title = obj.get_title()
            markup = obj.entry_page()
            wikipost(title, markup)


class Event(Wiki):
    """ The Event class """
    def __init__(self, json_data):
        super().__init__(json_data)
    
    def to_slug(self, prefix=None, suffix=None):
        """
        Generates and returns the Event's page link
        This overrides the parent class's to_slug(),
        so consider yourself warned.
        :param prefix: list of prefixes
        :param suffix: list of suffixes
        :type prefix: list
        :type suffix: list
        """
        title = self.get_title()
        # print(f"Event {self.id} slug: prefix({prefix}) suffix({suffix})")
        if self.id.startswith("tier"):
            tier = self.id.replace("tier", "")
            return f"[Tier {tier}]({self.id})"
        
        if prefix:
            prefixes = " ".join(prefix)
            output = f"Event: [{title}]({self.id}): {prefixes}"
        else:
            output = f"Event: [{title}]({self.id})"
        return output


class Furniture(Wiki):
    """ The Furniture class """
    def __init__(self, json_data):
        super().__init__(json_data)

    @staticmethod
    def category_page():
        """ Generate -/wikis/Furniture """
        nm = "Name"
        ta = "Tags"
        bm = "Base<br>Maximum"
        co = "Cost"
        bo = "Bonus"
        re = "Requires"
        output = \
            f"|{nm}|{ta}|{bm}|{co}|{bo}|{re}|\n" \
            f"|:---|:---|:---|:---|:---|:---|\n"
        tmp = []
        for furnishing_id in Data.furnitures.keys():
            furnishing = Data.furnitures[furnishing_id]
            tmp.append(furnishing.category_line() + "\n")
        tmp.sort()
        output += "".join(tmp)
        wikipost("Furniture", output)

    def category_line(self):
        """
        Transform this Furniture object into a line of Markdown text
        :return: string of gitlab markdown text
        """
        symbol = self.get_symbol()
        title = self.to_slug()
        if self.desc is not None and self.desc != "":
            title += f"<br>*{self.desc}*"
        tags = self.tags
        if tags is None:
            tags = ""
        else:
            if "," in tags:
                temp_tags = []
                for t in tags.split(','):
                    if t.startswith("t_"):
                        t = t.replace("t_", "")
                    temp_tags.append(t)
                temp_tags.sort()
                tags = ", ".join(temp_tags)
            else:
                if tags.startswith("t_"):
                    tags = tags.replace("t_", "")
            tags = f"*{tags}*"
        bmax = self.max
        if bmax is None:
            bmax = ""
        cost = data.id_list_to_wiki(self.get_run_costs(), style="single")
        bonus = data.id_list_to_wiki(self.get_mods(), style="single")
        require = self.get_requirements()
        return f"|{symbol} {title}|{tags}|{bmax}|{cost}|{bonus}|{require}|"


class Home(Wiki):
    """ The Home class """
    def __init__(self, json_data):
        super().__init__(json_data)
    
    @staticmethod
    def category_page():
        """ Generate -/wikis/Homes """
        output = \
            "|Name|Size|Tags|Cost|Effects|Requirements|\n" \
            "|:---|:--:|:---|:---|:------|:-----------|\n"
        tmp = []
        for home_id in Data.homes.keys():
            home = Data.homes[home_id]
            tmp.append(home.category_line() + "\n")
        tmp.sort()
        output += "".join(tmp)
        wikipost("Homes", output)

    def category_line(self):
        """
        Transform this Home object into a line of Markdown text for the Homes page
        :return: string of gitlab markdown text
        """
        symbol = self.get_symbol()
        title = self.to_slug()
        desc = self.desc
        if desc is not None:
            desc = f"<br>*{desc}*"
        size = self.mod["space.max"]
        tags = self.tags
        if tags is None:
            tags = ""
        else:
            if "," in tags:
                temp_tags = []
                for t in tags.split(','):
                    if t.startswith("t_"):
                        t = t.replace("t_", "")
                    temp_tags.append(t)
                temp_tags.sort()
                tags = ", ".join(temp_tags)
            else:
                if tags.startswith("t_"):
                    tags = tags.replace("t_", "")
            tags = f"*{tags}*"
        cost = Data.id_list_to_wiki(self.get_costs(), style="single")
        effect = Data.id_list_to_wiki(self.get_mods(), style="single")
        required = self.get_requirements()
        return f"|{title}{symbol}{desc}|{size}|{tags}|{cost}|{effect}|{required}|"

    def get_costs(self):
        """
        Get the costs of buying this Home
        :return: list of IDs
        """
        cost_list = []
        if self.cost is not None:
            if type(self.cost) == dict:
                for key, val in self.cost.items():
                    cost_list.append([str(val), key])
            elif type(self.cost) == int:
                cost_list.append([str(self.cost), "[Gold](gold)"])
            else:
                devmsg(f"Unknown type '{type(self.cost)}' for Home cost: {self.cost}")
                exit(1)
        # print(f"run cost for {self.id}: {cost_list}")
        return cost_list


class Locale(Wiki):
    """  The Locale Class """
    def __init__(self, json_data):
        super().__init__(json_data)
    
    def get_requires(self):
        """
        Get the things this locale needs to be unlocked
        :return: list of IDs
        """
        req_list = []
        import re
        if self.require is not None:
            res = re.split('&|>=|<=|>|<|=|\+', self.require)
            for re in res:
                re = re.replace("(", "").replace(")", "")
                if re == "":
                    continue
                if re.isnumeric():
                    continue
                if re.startswith("g."):
                    re = re.replace("g.", "")
                req_list.append(re)
        # print(f"require list: {req_list}")
        return req_list
    
    @staticmethod
    def category_page():
        """ Generate -/wikis/Locales """
        output = \
            "|Name|Length|Dist|Level|Required|Reward|Skill Exp|Max Increase|Unlocks|\n" \
            "|:---|:----:|:--:|:---:|:-------|:-----|:--------|:-----------|:------|\n"
        tmp = []
        for locale_id in Data.locales.keys():
            if locale_id == "loc_ettinmoors":
                continue  # skip this blank one
            locale = Data.locales[locale_id]
            tmp.append(locale.category_line() + "\n")
        tmp.sort()
        output += "".join(tmp)
        wikipost("Locales", output)
    
    def category_line(self):
        """
        Transform this Locale object into a line of Markdown text for the Locales page
        :return: string of gitlab markdown text
        """
        symbol = self.get_symbol()
        title = self.to_slug()
        length = self.get_length()
        dist = self.get_dist()
        level = self.get_level()
        required = self.get_requirements()
        if required == "":
            required = f"[Level](level) ≥ {level - 1}"
        rewards = Data.id_list_to_wiki(self.get_resource_rewards())
        exps = Data.id_list_to_wiki(self.get_exp_rewards())
        maxs = Data.id_list_to_wiki(self.get_max_rewards())
        unlocks = Data.id_list_to_wiki(self.get_unlocks())
        return f"|{title}<br>{symbol}|{length}|{dist}|{level}|{required}|{rewards}|{exps}|{maxs}|{unlocks}|"

    @classmethod
    def entry_pages(cls):
        """
        Generate a page for each Locale
        :return: None
        """
        for locale_id in Data.locales.keys():
            obj = Data.locales[locale_id]
            title = obj.get_title()
            # devmsg(f"Locale: {locale_id} ({title})")
            markup = obj.entry_page()
            wikipost(title, markup)


class Monster(Wiki):
    """ The Monster class """
    def __init__(self, json_data):
        super().__init__(json_data)

    @staticmethod
    def category_page():
        """ Generate -/wikis/Monsters """
        nm = "Name"
        lv = "Level"
        hp = "HP"
        db = "Defense<br>Bonus"
        re = "Regen"
        th = "To Hit<br>Bonus"
        sb = "Speed<br>Bonus"
        un = "Unique"
        at = "Attack"
        output = \
            f"|{nm}|{lv}|{hp}|{db}|{re}|{th}|{sb}|{un}|{at}|\n" \
            f"|:---|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|\n"
        tmp = []
        for monster_id in Data.monsters.keys():
            monster = Data.monsters[monster_id]
            tmp.append(monster.category_line() + "\n")
        tmp.sort()
        output += "".join(tmp)
        wikipost("Monsters", output)

    def category_line(self):
        """
        Transform this Monster object into a line of Markdown text for the Locales page
        :return: string of gitlab markdown text
        """
        sym = self.get_symbol()
        title = self.to_slug()
        nm = f"{sym} {title}"
        if self.desc is not None and self.desc != "":
            nm += f"<br>*{self.desc}*"
        lv = self.get_level()
        hp = self.hp
        df = self.defense
        re = self.regen
        th = self.tohit
        sb = self.speed
        un = "Yes" if self.unique else "No"
        at = "; ".join(self.get_attacks())
        return f"|{nm}|{lv}|{hp}|{df}|{re}|{th}|{sb}|{un}|{at}|"
    
    def get_attacks(self):
        """
        Format this monster's attack data
        :return: list of markdown strings
        """
        attacks = []
        if self.attack is None:
            return attacks
        # devmsg(f"{self.id} attack: {self.attack}")
        name = None
        kind = None
        if "dot" in self.attack:
            dot = self.attack["dot"]
            if "name" in dot and "kind" in dot and "dmg" in dot and "duration" in dot:
                nm = dot["name"].title()
                ki = dot["kind"]
                dm = dot["dmg"]
                du = dot["duration"]
                attacks.append(f"{nm}: {dm} {ki} damage for {du} seconds")
            elif "name" in dot:
                name = dot["name"]
            elif "adj" in dot:
                name = dot["adj"]
            if name is not None:
                attacks.append(f"Dot: {name}")
            
        if "name" in self.attack:
            name = self.attack["name"].title()
            dmg = None
            if "dmg" in self.attack:
                dmg = self.attack["dmg"]
            elif "dot" in self.attack and "dmg" in self.attack["dot"]:
                dmg = self.attack["dot"]["dmg"]
            if "kind" in self.attack:
                kind = self.attack["kind"]
            if kind is None and "type" in self.attack:
                kind = self.attack["type"]
            attacks.append(f"{name}: {dmg} {kind} damage")
        return attacks


class Player(Wiki):
    """ The Player class """
    def __init__(self, json_data):
        super().__init__(json_data)
    
    @classmethod
    def entry_pages(cls):
        """
        Generate a page for each Locale
        :return: None
        """
        for key in Data.player.keys():
            obj = Data.player[key]
            title = obj.get_title()
            markup = obj.entry_page()
            wikipost(title, markup)


class Potion(Wiki):
    """ The Potion class """
    def __init__(self, json_data):
        super().__init__(json_data)
    
    @classmethod
    def entry_pages(cls):
        """
        Generate a page for each Locale
        :return: None
        """
        for key in Data.potions.keys():
            obj = Data.potions[key]
            title = obj.get_title()
            markup = obj.entry_page()
            wikipost(title, markup)


class XXXXXXX:
    pass


class Upgrade(Wiki):
    """ The Upgrade class """
    def __init__(self, json_data):
        super().__init__(json_data)


class Resource(Wiki):
    """ The Resource class """
    def __init__(self, json_data):
        super().__init__(json_data)


class Task(Wiki):
    """ The Task class """
    def __init__(self, json_data):
        super().__init__(json_data)

    def get_requires(self):
        """
        Get the things this local needs to be unlocked
        :return: list of IDs
        """
        req_list = []
        import re
        if self.require is not None:
            if type(self.require) == list:
                req_list.append(self.require)
            else:
                # print(f"require: {self.require}")
                # print(f"type: {type(self.require)}")
                # print(f"task: {vars(self)}")
                res = re.split('&|>=|<=|>|<|=|\+|\|', self.require)
                for re in res:
                    re = re.replace("(", "").replace(")", "")
                    if re == "":
                        continue
                    if re.isnumeric():
                        continue
                    if re.startswith("g."):
                        re = re.replace("g.", "")
                    req_list.append(re)
        if self.need is not None:
            if type(self.need) == list:
                req_list.append(self.need)
            else:
                need = self.need.replace("(", "").replace(")", "").replace(">=", " ")\
                    .replace(">", " ").replace("||", " ").replace("<=", " ").replace("<", " ")

                needs = need.split()
                for n in needs:
                    if n == "":
                        continue
                    if n.isnumeric():
                        continue
                    if n.startswith("g."):
                        n = n.replace("g.", "")
                    req_list.append(n)
        # print(f"require list: {req_list}")
        return req_list


class Weapon(Wiki):
    """ The Weapon class """
    def __init__(self, json_data):
        super().__init__(json_data)


class Material(Wiki):
    """ The Material class """
    def __init__(self, json_data):
        super().__init__(json_data)


class Rare(Wiki):
    """ The Rare class """
    def __init__(self, json_data):
        super().__init__(json_data)


class Spell(Wiki):
    """ The Spell class """
    def __init__(self, json_data):
        super().__init__(json_data)


class Skill(Wiki):
    """ The Skill class """
    def __init__(self, json_data):
        super().__init__(json_data)

    def get_requires(self):
        """
        Get the things this local needs to be unlocked
        :return: list of IDs
        """
        req_list = []
        import re
        if self.require is not None:
            res = re.split('&|>=|<=|>|<|=|\+', self.require)
            for re in res:
                re = re.replace("(", "").replace(")", "")
                if re == "":
                    continue
                if re.isnumeric():
                    continue
                if re.startswith("g."):
                    re = re.replace("g.", "")
                req_list.append(re)
        # print(f"require list: {req_list}")
        return req_list


class Reagent(Wiki):
    """ The Reagent class """
    def __init__(self, json_data):
        super().__init__(json_data)
            

class Stressor(Wiki):
    """ The Stressor class """
    def __init__(self, json_data):
        super().__init__(json_data)


class Tags:
    """ Store tags here in this class """
    storage = dict()
    
    def __init__(self):
        pass
    
    @classmethod
    def add(cls, tag, object_id):
        if Data.tags is None:
            Data.tags = Tags()
        if tag in cls.storage:
            if object_id in cls.storage[tag]:
                devmsg(f"'{object_id}' already in tag '{tag}'")
            else:
                cls.storage[tag].append(object_id)
        else:
            cls.storage[tag] = [object_id]
        return

    @classmethod
    def category_page(cls):
        """ Tags page """
        
        output = "|Tag Name|Tagged Items|\n"
        output += "|:-------|:-----------|\n"
        sorted_keys = list(cls.storage.keys())
        sorted_keys.sort()
        item_list = []
        for key in sorted_keys:
            line = f"|{key}|"
            items = cls.storage[key]
            items.sort()
            line += Data.id_list_to_wiki(items)
            line += "|"
            item_list.append(line)
            
        output += "<br>\n".join(item_list)
        # print(output)
        wikipost("Tags", output)


def wikipost(title, content):
    base_url = "https://gitlab.com/api/v4/projects/22897802/wikis"
    headers = {"PRIVATE-TOKEN": os.getenv("ARCANUM_WIKI_KEY")}
    slug = title.replace(" ", "-").replace("?", "%3F")
    target = base_url + "/" + slug
    
    # Check if slug already exists
    # devmsg(f"Checking if '{slug}' already exists..")
    r = requests.get(target)
    # print(f"r:({r}) text:({r.text})")
    # devmsg(f"status code is {r.status_code}...")
    if r.status_code == 404:
        # devmsg(f"'{slug}' doesn't exist, creating..")
        # Create a new page with a slug title, so slug generates properly
        payload = {"format": "markdown", "title": title, "content": "tbd"}
        r = requests.post(base_url, headers=headers, data=payload)
        # print(f"Create results: [{r}] ({r.text})")
        if r.status_code != 201:
            devmsg(f"slug({slug}) title({title})")
            devmsg(f"Response: {r} {r.text}")
            devmsg(f"content: {content}")
            exit(1)

    # Overwrite content (possibly new tbd content) with proper content
    # devmsg(f"'{slug}' exists, updating...")
    payload = {
        "format": "markdown",
        "title": title,
        "content": content
    }
    r = requests.put(target, headers=headers, data=payload)
    if r.status_code != 200:
        devmsg(f"slug({slug}) title({title})")
        devmsg(f"Response: {r} {r.text}")
        devmsg(f"content: {content}")
        exit(1)


def devmsg(msg):
    ts = datetime.now().time()
    c = sys._getframe().f_back.f_code.co_name
    f = path.basename(sys._getframe().f_back.f_code.co_filename)
    l = sys._getframe().f_back.f_lineno
    spc = ' ' * sys._getframe().f_back.f_code.co_stacksize
    if msg == 'trace':
        print(f'{ts}{spc}{f} {c} {l}: tracing...')
        traceback.print_stack()
        return
    # print(f'{ts}{spc}{f} {c} {l}: {msg}')
    print(f'{ts}{spc}{c} ({l}): {msg}')
    return


data = Data()
data.load_base_data()
data.load_module_data()

data.generate_pages()
# devmsg(f"tags: {Tags.storage}")

print("Done!")
